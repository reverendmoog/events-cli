# events-cli #

A shell script for replaying events from a csv file

## What's included ##

* events-cli - A bourne shell script that replays events from a file
* service-cli - A bourne shell script that iterates over replay files in the `services` directory
* gen-files - A tool to generate service replay files from a list (documented in comments).
* lib/events.csv - A csv file containing events

## Usage ##
`events-cli [-f <file.csv>] [debug]`
`services-cli [debug]`

**NOTE:** You will need to create a file called `.events-cli.rc` containing your API key. It's a shell file, so it will accept comments, and should be in the form:
```
# Demo
export API_KEY='1234abc-1234-abcd-1234-abcd1234'
```

### events-cli ###

`$ events-cli`

This will look for a default `lib/events.csv` file, and replay the events, with cadence.

`$ events-cli debug`

Will not send the events, but will output them to the screen for reference.

`$ events-cli -f test.csv`

Will read from a specific _csv_ file.

### services-cli ###

`$ services-cli`

This will itterate over any CSV files in the `lib/services` directory in _ASCIIbetical_ order.

If a file with a name containing "sleep" (note lowercase) is found, it will introduce a delay in seconds read from that file.

### lib/events.csv ###

The event file format is common accross all files. The column order is critical. Example:
```
SOURCE,DELAY,SEVERITY,CHECK,MANAGER,SERVICE,DESCRIPTION,TAGS
c-24a0d48a4e-121,1,2,ELB Connections,Splunk,billing,Error 5xx request count > x 100 | c-23a0d48a4m-121,"cluster":"checkout-w-elb-01"
```
Most columns should be self-explanatory as they map to the Moogsoft Cloud Event API endpoint schema.

**NOTE:** Since this is a CSV file, strings cannot contain commas.

`DELAY` -  This is a delay, in seconds, before the event is sent. This is used to introduce cadence.  
`TAGS` - Tags can added. To add multiple tags, simply add multiple quoted key value pairs. e.g:  
```
"foo":"bar""humble":"pie"
```
